/*
 * my_first.char.c
 *
 * Copyright (C) Niv B.
 *
 */


/***********************************************************************************************
 * libraries
 ***********************************************************************************************/

#include "my_first_char.h"




/***********************************************************************************************
 * globals
 ***********************************************************************************************/

struct mutex gLock; 
struct cdev cdev;

int major;
int minor;
char *kernel_buff = NULL;
int gCount = SIZE_KERNEL_BUFFER;
int gInput0; // input (1st) from cmd



/***********************************************************************************************
 * file API structure initialization
 ***********************************************************************************************/
struct file_operations char_fops = {
	.owner =    THIS_MODULE,
	.read =     char_read,
	.write =    char_write,
	.open =     char_open,
	.release =  char_release,
};


/***********************************************************************************************
 * cmd input
 ***********************************************************************************************/

module_param(gInput0, int, S_IRUGO);




/***********************************************************************************************
 * module definitions
 ***********************************************************************************************/

MODULE_AUTHOR("NivB");
MODULE_LICENSE("GPL");


/***********************************************************************************************
 * file (char device) API
 ***********************************************************************************************/

/**
 * allocates the memory of kernel_buff if it wasn't already allocated.
 * this could'v be done in module creation instead but stays here for future implementations
 */
int char_open(struct inode *inode, struct file *sfile) {
	printk(KERN_NOTICE "char_open()\n");
	
	mutex_lock(&gLock);	
	
	// we won't allocate memory again if we already did
	if (kernel_buff==NULL) {
		// can sleep while mallocing. this is ok as long as we're not in interrupt function, which we aren't
	    	kernel_buff = kmalloc(gCount, GFP_KERNEL);

		printk(KERN_NOTICE "char_open(): kernel_buff=0x%p\n",kernel_buff);	
		if (kernel_buff==NULL) {
			printk(KERN_ERR "char_open(): kmalloc error\n");
			return -1;
		}
	}

	mutex_unlock(&gLock);
	
	return 0;
}

/**********************************************************************************************/

int char_release(struct inode *inode, struct file *sfile) {
	printk(KERN_NOTICE "char_release()\n");
	
	mutex_lock(&gLock);	
	
	// release the allocated buffer on kernel space
	if(kernel_buff!=NULL) {
		kfree(kernel_buff);
		kernel_buff = NULL;
	}

	mutex_unlock(&gLock);

	return 0;
}

/**********************************************************************************************/

ssize_t char_read(struct file *sfile, char __user *buf, size_t count, loff_t *f_pos) {
	
	ssize_t retval = 0;
	
	printk(KERN_NOTICE "char_read(): count=%d\n", count);
	
	// aquire lock for the global resource (kernel_buff)
	mutex_lock(&gLock);	
	
	// copy from kernel buffer to user_buffer
	retval = copy_to_user(buf, kernel_buff, count);

	// release lock for the global resource (kernel_buff)
	mutex_unlock(&gLock); 

	return (retval != 0) ? (-EFAULT) : (count); // return (-EFAULT) if error occured, return count otherwise
}

/**********************************************************************************************/

ssize_t char_write(struct file *sfile, const char __user *buf, size_t count, loff_t *f_pos) {
	ssize_t retval = 0;

	printk(KERN_NOTICE "char_write(): count=%d\n", count);
		
	// aquire lock for the global resource (kernel_buff)
	mutex_lock(&gLock);	
	
	// copy from user to kernel buffer
	retval = copy_from_user(kernel_buff, buf, count);
	printk(KERN_NOTICE "char_write(): copy_to_user return %d\n",retval);

	// release lock for the global resource (kernel_buff)
	mutex_unlock(&gLock); 

	return (retval != 0) ? (-EFAULT) : (count); // return (-EFAULT) if error occured, return count otherwise
}

/**********************************************************************************************/



/***********************************************************************************************
 * driver exit point, called on `rmmod` command
 * @NOTE: why unregister_chrdev_region is needed ?
 ***********************************************************************************************/
void exit_driver(void) {
	int device_amount = 1;
	dev_t devno = MKDEV(major, minor);

	// tell system to remove this char device
	cdev_del(&cdev); 
	unregister_chrdev_region(devno, device_amount); // **cleanup_module is never called if registering failed
	printk(KERN_NOTICE "char device '%s' unregistered",NAME_DEVICE);
}



/***********************************************************************************************
 * private (static) functions
 ***********************************************************************************************/

/*
 * Set up the char_dev structure for this device.
 */
static void setup_cdev(void) {
	int err;
	int devices_amount = 1;
	int devno = MKDEV(major, minor);
    
	cdev_init(&cdev, &char_fops);
	cdev.owner = THIS_MODULE;
	cdev.ops = &char_fops;
	err = cdev_add (&cdev, devno, devices_amount);
	if (err) {
		printk(KERN_NOTICE "ERROR: cdev_add return %d", err);
	}
}


/***********************************************************************************************
 * entry point of the module, called on `mkmod`
 ***********************************************************************************************/
int init_driver(void) {
	int result;
	dev_t dev = 0;

	result = alloc_chrdev_region(&dev, minor, 1, NAME_DEVICE);
	major = MAJOR(dev);
	printk(KERN_WARNING "init_driver():  driver major = %d\n", major);
	
	
	if (result < 0) {
		printk(KERN_WARNING "init_driver(): REGISTRATION FAILURE %d\n", major);
		exit_driver();
		return result;
	}

	setup_cdev(); // initialize the char device

	mutex_init(&gLock); // dynamic initialization
	
	return 0; // succeed
}




/***********************************************************************************************
 * reguster entry and exit points of the driver
 ***********************************************************************************************/
module_init(init_driver);
module_exit(exit_driver);
